<?php

/**
 * @file
 * Tokens - Defines custom tokens for media audio entities.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function rfn_media_audio_token_info() {

  $type = [
    'name' => t('RFN Media Audio Tokens'),
    'description' => t('Tokens for media audio entities.'),
  ];

  $node['recording_for_track'] = [
    'name' => t('Recording for Track'),
    'description' => t('The Recording this track belongs to'),
  ];

  $node['artist_for_track'] = [
    'name' => t('Artist for Track'),
    'description' => t('The Artist this track belongs to'),
  ];

  return [
    'types' => ['rfn_media_item' => $type],
    'tokens' => ['rfn_media_item' => $node],
  ];
}

/**
 * Implements hook_tokens().
 */
function rfn_media_audio_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $replacements = [];

  if ($type == 'rfn_media_item' && !empty($data['node'])) {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'recording_for_track':

          // Fetch the recording this track belongs to.
          $albumNodes = \Drupal::entityTypeManager()
            ->getStorage('node')
            ->loadByProperties(['field_media_items' => $data['node']->id()]);

          $album = array_shift($albumNodes);
          if ($album) {

            if ($album->id()) {
              $replacements[$original] = $album->getTitle();
            }
          }
          break;

        case 'artist_for_track':
          // Fetch the recording this track belongs to.
          $albumNodes = \Drupal::entityTypeManager()
            ->getStorage('node')
            ->loadByProperties(['field_media_items' => $data['node']->id()]);

          $album = array_shift($albumNodes);
          if ($album != NULL) {
            // $artists = $album->get('field_artists');
            $artist = $album->get('field_artists')->first()->get('entity')->getTarget()->getValue();
            $replacements[$original] = $artist->getTitle();
          }
          break;

      }
    }

  }
  return $replacements;
}
